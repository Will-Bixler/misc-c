﻿/*

Author:     William Bixler
Created:    9/20/18
Bugs:       None

Description:
    The code asks the user for a text string and records it. It looks for vowels and removes them from the string.
    The vowels get arranged alphabetically and inserted to the begining of the string.

*/

using System;
using System.Collections.Generic;

namespace Vowel_Arranger {
    class Program {
        static void Main(string[] args) {
            Console.WriteLine("What is the string?");

            // Get user input phrase
            var phrase = Console.ReadLine();

            // Define list of vowels
            var vowelsList = new List<char> { 'a', 'e', 'i', 'o', 'u', 'y' };
            var vowelsFromPhrase = new List<char>();

            if (phrase != null)
                // Check each char in phrase
                for (var i = 0; i < phrase.Length; i++) {
                    // If char is in the vowel list
                    if (!vowelsList.Contains(phrase.ToLower()[i])) continue;

                    // Add vowel to vowels from phrase list and remove from string
                    vowelsFromPhrase.Add(phrase[i]);
                    phrase = phrase.Remove(i, 1);
                }

            // Order vowel list alphabetically
            vowelsFromPhrase.Sort();

            // Convert vowels from phrase list to string
            var vowelString = "";
            vowelsFromPhrase.ForEach(i => vowelString += i);

            // Create new string with vowels in front and consonants in back
            var newPhrase = vowelString + phrase;

            // Output arranged phrase and vowel list
            Console.WriteLine("New phrase: \t{0}", newPhrase);
            Console.WriteLine("Vowel List: {0}", vowelString);

            Console.Write("Press any key to exit...");
            Console.ReadKey();

        }
    }
}
